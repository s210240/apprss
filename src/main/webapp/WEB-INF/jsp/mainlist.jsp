<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/jumbotron.css" rel="stylesheet">
<title>Список каналов</title>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Application RSS</a>
			</div>
		</div>
	</div>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
		<div class="container">
			<p>Простое приложение для управления RSS</p>
		</div>
	</div>

	<div class="container">
		<!-- Example row of columns -->
		<div class="row">
			<div class="col-md-12">
				<h3>Список каналов</h3>
				<table class="table table-striped">
					<tr>
						<td>Название</td>
						<td>Адрес</td>
						<td>Удаление</td>
					</tr>
					<c:forEach items="${mainrss}" var="mainrss">
						<tr>
							<td><c:out value="${mainrss.key}"></c:out></td>
							<td><a href="${mainrss.value}" target="_blank"><c:out
										value="${mainrss.value}"></c:out></a></td>
							<td><a href="#">Удалить</a></td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
		<hr>
		<footer>
			<p>&copy; Roman 2013</p>
		</footer>
	</div>

	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<%-- <p>fgdgdfgfdg</p>
	<c:forEach items="${users}" var="users">
		<p>
			<c:out value="${users.email}"></c:out>
		</p>
	</c:forEach> --%>
</body>
</html>