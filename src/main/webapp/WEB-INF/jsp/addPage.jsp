<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/jumbotron.css" rel="stylesheet">
<title>Добавление канала</title>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Application RSS</a>
			</div>
		</div>
	</div>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
		<div class="container">
			<p>Простое приложение для управления RSS</p>
		</div>
	</div>

	<div class="container">
		<!-- Example row of columns -->
		<div class="row">
			<div class="col-md-12">
				<h3>Добавление канала</h3>
				<p>Добавление имени и адреса нового канала</p>
				<form:form method="POST" action="/TestRss/addRSS">

					<p>
						<form:label path="name">Название канала</form:label>
					</p>
					<p>
						<form:input path="name" />
					</p>

					<p>
						<form:label path="adress">Адрес канала</form:label>
					</p>
					<p>
						<form:input path="adress" />
					</p>
					<input type="submit" value="Добавить канал" />

				</form:form>
			</div>
		</div>

		<hr>

		<footer>
			<p>&copy; Roman 2013</p>
		</footer>
	</div>

	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>