package com.kurylko;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

public class MainListData {
	private Map<Long, Users> users = new HashMap<Long, Users>();
	private AtomicLong sequence = new AtomicLong(0);

	public MainListData() {
		saveUser(new Users("john", "John Smith", "jsmith@mail.com", new Date(),
				true));
		saveUser(new Users("steve", "Steve Brown", "sbrown@mail.com",
				new Date(), true));
	}

	public List<Users> getAllUsers() {
		return new ArrayList<Users>(users.values());
	}

	public Users findUserById(Long id) {
		return users.get(id);
	}

	public Users saveUser(Users user) {
		if (user.getId() == null) {
			user.setId(sequence.getAndIncrement());
		}
		users.put(user.getId(), user);
		return user;
	}
}
