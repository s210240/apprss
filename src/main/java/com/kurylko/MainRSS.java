package com.kurylko;

public class MainRSS {
	private int id;
	private String rssName;
	private String rssAdress;

	public MainRSS() {
	}

	public MainRSS(String mName, String mAdress) {
		this.rssName = mName;
		this.rssAdress = mAdress;
	}
	
	/*@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_seq_gen")
	@SequenceGenerator(name = "users_seq_gen", sequenceName = "auto_id_mainrss")*/
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return rssName;
	}

	public void setName(String rss_Name) {
		this.rssName = rss_Name;
	}

	public String getAdress() {
		return rssAdress;
	}

	public void setAdress(String rss_Adress) {
		this.rssAdress = rss_Adress;
	}
}
