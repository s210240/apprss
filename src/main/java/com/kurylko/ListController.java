package com.kurylko;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/mainlist")
public class ListController {
	@RequestMapping(method = RequestMethod.GET)
	public String mainList(ModelMap model) {
		BaseRSS BR = new BaseRSS();
		BR.listEmployees();
		
		MainListData userDao = new MainListData();
		
		//model.addAttribute("users", userDao.getAllUsers());

		model.addAttribute("mainrss", BaseRSS.mainrss);
		return "mainlist";
	}
}

