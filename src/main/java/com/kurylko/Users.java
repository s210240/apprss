package com.kurylko;

import java.util.Date;

public class Users {
	private Long id;
	private String login;
	private String name;
	private String email;
	private Date birthDate;
	private Boolean active;

	public Users(String login1, String name1, String email1, Date birth,
			boolean active1) {
		this.login = login1;
		this.name = name1;
		this.email = email1;
		this.birthDate = birth;
		this.active = active1;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
