package com.kurylko;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AddController {
	@RequestMapping(value = "/addPage", method = RequestMethod.GET)
	public ModelAndView addPage() {
		return new ModelAndView("addPage", "command", new MainRSS());
	}

	@RequestMapping(value = "/addRSS", method = RequestMethod.POST)
	public String addRSS(@ModelAttribute("TestRSS") MainRSS addrss,
			ModelMap model) {
		
		model.addAttribute("name", addrss.getName());
		model.addAttribute("adress", addrss.getAdress());
		
		
		BaseRSS BC = new BaseRSS();
		
		BC.addEmployee(addrss.getName(), addrss.getAdress());
		
		return "result";
	}
}
